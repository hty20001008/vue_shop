# vue_shop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## 淘宝镜像
```
npm install -g cnpm --registry=https://registry.npmmirror.com
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
